FROM golang:latest AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 go build -o server

FROM gcr.io/distroless/static

WORKDIR /

ENV ENTRYPOINT=src/main.ts
ENV PLATFORM=vue

COPY --from=build /app/server /server

USER nonroot:nonroot

EXPOSE 8080

VOLUME [ "/dist", "/static" ]

ENTRYPOINT [ "/server" ]