package main

import (
	"html/template"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
	vueglue "github.com/torenware/vite-go"
)

var vueGlue *vueglue.VueGlue
var e *echo.Echo

func main() {
	viper.AutomaticEnv()
	viper.SetDefault("ENTRYPOINT", "src/main.ts")
	viper.SetDefault("PLATFORM", "vue")
	viper.SetDefault("PORT", ":8080")

	e = echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	config := &vueglue.ViteConfig{
		Environment: "production",
		AssetsPath:  "dist",
		EntryPoint:  viper.GetString("ENTRYPOINT"),
		Platform:    viper.GetString("PLATFORM"),
		FS:          os.DirFS("."),
	}

	glue, err := vueglue.NewVueGlue(config)
	if err != nil {
		e.Logger.Error(err)
		return
	}

	vueGlue = glue

	fsHandler, err := glue.FileServer()
	if err != nil {
		e.Logger.Error("Could not set up static file server", err)
		return
	}

	e.Static("/static", "static")

	e.Any("/assets/*", echo.WrapHandler(fsHandler))

	e.Any("/*", echo.WrapHandler(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		t, err := template.ParseFS(glue.DistFS, "dist/template.tmpl")
		if err != nil {
			e.Logger.Error(err)
		}

		if err := t.Execute(w, vueGlue); err != nil {
			e.Logger.Fatal(err)
		}
	})))

	e.Logger.Fatal(e.Start(viper.GetString("PORT")))
}
