# Artrinix Web Server

This is our small web server made in [Go](https://go.dev/) with the [Echo](https://echo.labstack.com/) framework and [Vite-Go](https://github.com/torenware/vite-go).

Built to spin up vue single page applications with as little overhead as possible.

GitLab CI should build Docker images for every commit and tag the latest

## Setup 

### ENV

> **Note**
> If you plan to manually build this and run it outside of a Docker container, it requires a `dist` and `static` folder in the same directory as the executable.

```
ENTRYPOINT=src/main.ts
PLATFORM=vue|react|svelte
```

### Volumes

> **Note**
> You will need to add a [template.tmpl](https://github.com/torenware/vite-go#templates) to your `dist` folder, this can be done easily by adding it to your `public` in your project.

`/dist` This should be the `dist` from your vite build.
`/static` For any static assets you want to add live, this is optional and is available at <ip>/static.
